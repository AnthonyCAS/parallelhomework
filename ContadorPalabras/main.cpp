#include <iostream>
#include <vector>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include "clases.h"
#include <stdio.h>
#include <sys/time.h> //gettimeofday()
using namespace std;

int opcion(){
    int op;
    system("clear");
      cout<<"\n\t ÉÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ»";
      cout<<"\n\t º    "<<" CONTADOR DE PALABRAS           º";
      cout<<"\n\t º    "<<" By Anthony Ccapira Avendano    º";
      cout<<"\n\t ÈÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍŒ";
      cout<<"\n\t ÉÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ»";
      cout<<"\n\t º   <---------->Menu:<----------->   º";
      cout<<"\n\t º   ------------------------------   º";
      cout<<"\n\t º      1.- Contar (Secuencial)       º";
      cout<<"\n\t º      2.- Contar (Paralelo)         º";
      cout<<"\n\t º      3.- Salir  ---------->        º";
      cout<<"\n\t ÈÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍŒ";
      cout << "\n\t\t <-->  Contador:        <-->  " << endl;
      cout << "\n\t\t <-->  OPCION: ";
    cin>>op;
    return op;
}

string name = "cien.txt";
ContadorT Contador(name); 
//Funciones para crear las loterias

void contarS(){
	struct timeval comienzo, final;
	double tiempo;
	gettimeofday(&comienzo, NULL);
    cout << "\nSe contaron: " << Contador.contarS() << " palabras. ";
    
	gettimeofday(&final, NULL);
	tiempo= (final.tv_sec - comienzo.tv_sec)*1000 + (final.tv_usec - comienzo.tv_usec)/1000.0;
    printf("Tiempo Total: %g milisegundos\n", tiempo);
}

void contarP(){
    int nthread;
    cout <<"\nNumero de Threads: "; 
    cin >> nthread;
    cout << "\nSe contaron: " << Contador.contarP(nthread) << " palabras. ";
}

int main()
{
    int o;
    
    void (*func[2])();
    func[0]=&contarS;
    func[1]=&contarP;
    do{
        //system("clear");
        o=opcion();
        if(o<1||o>3){
                cout<<"\n Opcion incorrecta";
                //cin.ignore().get();
                continue;
        }
        if(o != 3)
            func[o-1]();

    }while(o!=3);
    cout<<"\n Adios\n";
    return 0;
}