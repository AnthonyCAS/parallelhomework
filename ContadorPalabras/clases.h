#ifndef CLASES_H_INCLUDED
#define CLASES_H_INCLUDED
#include <vector>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdio.h>
#include <sys/time.h> //gettimeofday()
using namespace std;

void *parseP(void *param) {
      string *cadena_temp;
      cadena_temp = (string *) param;

      int *cont = new int;
      *cont = 0;
      string aux;
      string cadena = *cadena_temp;
      //cout << "\n hilo palabra: " << cadena;
        if(cadena.empty()){
         // cout << "\n vacio"<<endl;
           pthread_exit(cont);
        }
            
        size_t x;
        int tam = cadena.length();
        int pos = 0;
            //cout<<"\n cadena: "<<cadena<< " tam: "<< tam <<endl;
                x=cadena.find(" ");
                aux=cadena.substr(pos,x-pos);
                (*cont)++;
            while(x!=std::string::npos){
                pos=x+1;
                //cout << "\n warning pos: " <<pos << ", x:"<<x <<endl;
                x=cadena.find(" ",x+2);
                aux = cadena.substr(pos,x-pos);
                //cout << aux <<endl;
                        (*cont)++;
            }
            pthread_exit(cont);
}

class ContadorT{
    string name;
    long int cont;
    pthread_t * threads;
 public:
     ContadorT(string name){
         this->name=name;
     }

    long int parseS(string cadena){
 
        if(cadena.empty())
            return 0;
        long int cont=0; string aux;
        size_t x;
        long int tam = cadena.length();
        long int pos = 0;
            //cout<<"\n cadena: "<<cadena<< " tam: "<< tam <<endl;
                x=cadena.find(" ");
                aux=cadena.substr(pos,x-pos);
                cont++;
            while(x!=std::string::npos){
                pos=x+1;
                //cout << "\n warning pos: " <<pos << ", x:"<<x <<endl;
                x=cadena.find(" ",x+2);
                aux = cadena.substr(pos,x-pos);
                //cout << aux <<endl;
                        cont++;
            }

            return cont;
    }


    long int contarS(){

      long int contador_=0;
      ifstream read(name.data(), ios::in );
      string fila; double tiempo;
      cout << "\nLeyendo el Texto\n\n";
      int lineas = 0;
      //time
      struct timeval comienzo, final;
      gettimeofday(&comienzo, NULL);
      while(!read.eof()){
            getline(read, fila);
            contador_ += parseS(fila);
            lineas++;
            //cout << fila <<endl;        
      }
      gettimeofday(&final, NULL);
      tiempo= (final.tv_sec - comienzo.tv_sec)*1000 + (final.tv_usec - comienzo.tv_usec)/1000.0;
      printf("Tiempo Serie: %g milisegundos\n", tiempo);
      read.close();
      cout << "\nlineas: " << lineas << endl;
      return contador_;
    }

    int contarP(int tam){

      void *res;
      //int *val;
      //iniciando los threads
      threads = new pthread_t[tam];
      int contador_= 0;
      ifstream read(name.data(), ios::in );
      string fila;
      cout << "\nLeyendo el Texto\n\n";
      int delimitador = 0;
      vector<string> buffer;
      //time
      struct timeval comienzo, final;
	  gettimeofday(&comienzo, NULL);

      while(!read.eof()){
                getline(read, fila);
                if(delimitador < tam && !fila.empty()){
                    buffer.push_back(fila);
                    delimitador ++;
                }
                if(delimitador == tam ){
                    for(int i =0; i < delimitador; ++i){
                        pthread_create (&threads[i], NULL, parseP, &buffer.at(i));
                    }
                    for(int i =0; i < delimitador; ++i){
                        pthread_join(threads[i], &res);
                        int *val = (int*) res;
                        contador_ += *val;
                    }
                    delimitador = 0;

                    buffer.clear();
                }
      }
      for(int i =0; i < delimitador; ++i){
          pthread_create (&threads[i], NULL, parseP, &buffer.at(i));
      }
      for(int i =0; i < delimitador; ++i){
          pthread_join(threads[i], &res);
          int *val = (int*) res;
          contador_ += *val;
      }
      gettimeofday(&final, NULL);
      double tiempo;
      tiempo= (final.tv_sec - comienzo.tv_sec)*1000 + (final.tv_usec - comienzo.tv_usec)/1000.0;
   	  printf("Tiempo Paral: %g milisegundos\n", tiempo);
      buffer.clear();
      //deleting
      delete []threads;
      //delete val;

      read.close();

      return contador_;
    }

/*    void escribir(long int result,string cadena){
      stringstream oss;
      oss << result;
      ofstream write(cadena.data(), ios::out );
      write<<result;

      write.close();
    }*/

};


#endif // CLASES_H_INCLUDED