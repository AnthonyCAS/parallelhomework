#include <stdio.h>
#include <stdlib.h>
#include "pthread.h"
#include <time.h>
#include <iostream>
using namespace std;
//inicializo variables
#define limitdown 1  //variable para el limite inferior de los numeros random
#define limitup 10  //variable para el limite superior de los numeros random
//Aqui declaro mis matrices A, B, C, D y las auxliliares para guardar la multiplicacion
//pro1 guarda A*B
//pro2 guarda C*D, ademas las variables fil y col guardan las dimensiones de la matriz
	int **matA; 
	int **matB;
	int **matC;
	int **matD;
	int **pro1;
	int **pro2;
	int fil=4,col=4;

void imprimir(int **matriz);
void *producto(void *param);
void *suma(void *param);
//esta estructura la uso para dividir tareas entre los threads, en este caso solo divido las operacines por filas
typedef struct _delimitador {
  int inicio; //inicio fila
  int fin;    //fin fila
} delimitador;

//obtengo aleatorios
void getvalores(){
	for(int i=0;i<fil;++i){
		for(int j=0;j<col;++j){
			matA[i][j]=i+j;
			matB[i][j]=i+j*2;
			matC[i][j]=2*i+j*3;
			matD[i][j]=2*i+j;
		}
	}
}
//inicializo las dimensiones de la matriz dinamica
void iniciar(){
	matA = new int* [fil];
	matB = new int* [fil];
	matC = new int* [fil];
	matD = new int* [fil];
	pro1 = new int* [fil];
	pro2 = new int* [fil];
	for(int u=0; u<fil; ++u){
		matA[u] = new int [col];
		matB[u] = new int [col];
		matC[u] = new int [col];
		matD[u] = new int [col];
		pro1[u] = new int [col];
		pro2[u] = new int [col];
	}

}

int main() {

	pthread_t thr1, thr2; //creo 2 threads
	int id_thread0=0;
	int id_thread1=1;
	srand(time(NULL)); //genero la semilla para numeros aleatorios

    //opcional para que el usuario ingrese las dimensiones
	//cout <<"\n Ingrese el tamanho de la Fila Matriz: "; cin >> fil;
	//cout <<"\n Ingrese el tamanho de la Columna Matriz: "; cin >> col;
	iniciar();  //la funcion iniciar, inicializa las dimensiones de las cuatro matrices
	getvalores();   //la funcion getvalores,asigna valores de las cuatro matrices, segun el enunciado

	cout << "\n Imprimiendo la MatrizA antes de sumar" << endl;
	imprimir(matA);
	cout << "\n Imprimiendo la MatrizB antes de sumar" << endl;
	imprimir(matB);
	cout << "\n Imprimiendo la MatrizC antes de sumar" << endl;
	imprimir(matC);
	cout << "\n Imprimiendo la MatrizD antes de sumar" << endl;
	imprimir(matD);
    //creo los delimitadores, para dividir las tareas entre los threads por filas
	delimitador dthread1;
  	delimitador dthread2;
    //el primer delimitador opera la mitad superior de la matriz
	dthread1.inicio = 0;
	dthread1.fin = fil/2;
    //el segundo delimitador opera la mitad restante
	dthread2.inicio = dthread1.fin;
    dthread2.fin = fil;

	cout << "\n Multiplicando Matrices con Hilos";
//Creo threads para multiplicar A*B Y C*D ambos thread se dividen el trabajo
  pthread_create (&thr1, NULL, producto, &dthread1);
  pthread_create (&thr2, NULL, producto, &dthread2);
//espero a que terminen para luego sumar
  pthread_join(thr1, NULL);
  pthread_join(thr2, NULL);

	cout << "\n Imprimiendo Matriz resultante X1 = A*B\n";
	imprimir(pro1);

	cout << "\n Imprimiendo Matriz resultante X2 = C*D\n";
	imprimir(pro2);
	//finalmente sumo las matrices resultantes, pro1+pro2, aqui tambien uso los mismos delimitadores, porque todas las matrices tienen la misma dimension
	cout <<"\n SUMA FINAL R = X1 +X2:" <<endl;
	pthread_create (&thr1, NULL, suma, &dthread1);
	pthread_create (&thr2, NULL, suma, &dthread2);
  pthread_join(thr1, NULL);
  pthread_join(thr2, NULL);
	return 0;
}
//producto, trato de hacer un forall de cpar con esos delimitadores, solo apra 2 threads
void *producto(void *param) {
  int fila_ini, fila_fin;
  delimitador *datos;
//obtengo el delimitador, previo cast
  datos = (delimitador *) param;
//inicializo las variables para definir el rango
  fila_ini = datos -> inicio;
  fila_fin = datos -> fin;

//producto de matrices, 
  for (int i = fila_ini; i < fila_fin; ++i){
	    for (int j = 0; j < fil; ++j)	{
			pro1[i][j] = 0;
			pro2[i][j] = 0;
			for (int k = 0; k < col; ++k){
			  pro1[i][j] = pro1[i][j] + matA[i][k]*matB[k][j];
			  pro2[i][j] = pro2[i][j] + matC[i][k]*matD[k][j];
			}
		}
	}
}
//funcion suma, parecida a la multiplicacion
void *suma(void *param) {
  int fila_ini, fila_fin;
  delimitador *datos;
//obtengo delimitador
  datos = (delimitador *) param;

  fila_ini = datos -> inicio;
  fila_fin = datos -> fin;
  int x[fil][col]; //creo una matriz para obtener el resultado final
    //suma de matrices
	for (int i = fila_ini; i < fila_fin; i++){
	    for (int j = 0; j < col; ++j) {
			x[i][j] = pro1[i][j] + pro2[i][j];
			cout << " " << x[i][j];
		}
		cout << "\n";
	}
	pthread_exit(NULL);
}

//solo para imprimir en consola
void imprimir(int **matriz) {
	for(int i=0;i<fil;++i) {
		for(int j=0;j<col;++j)
			cout << matriz[i][j] << " ";
		cout << endl;
	}
	printf("\n");
}
