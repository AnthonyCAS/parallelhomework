#include <stdio.h>
#include <stdlib.h>
#include "pthread.h"
#include <time.h>
#include <iostream>
using namespace std;
//inicializo variables
#define limitdown 1
#define limitup 10
	int **matA;
	int **matB;
	int **matC;
	int fil=5,col=4;

void imprimir(int **matriz);
void *suma(void *param);

//lo uso para dividir tareas a los threads, en este caso solo divido filas
typedef struct _delimitador {
  int inicio;
  int fin;
} delimitador;

//obtengo aleatorios
void getramdon(int **matriz){
	for(int i=0;i<fil;++i){
		for(int j=0;j<col;++j){
			matriz[i][j]=rand()%(limitup-limitdown)+limitdown;
		}
	}
}
//inicializo las dimensiones de la matriz dinamica
void iniciar(int** &ma){
	ma = new int* [fil];
	for(int u=0; u<fil; ++u)
		ma[u] = new int [col];

}
int main() {

	pthread_t thr1, thr2;
	int id_thread0=0;
	int id_thread1=1;
	srand(time(NULL));


	//cout <<"\n Ingrese el tamanho de la Fila Matriz: "; cin >> fil;
	//cout <<"\n Ingrese el tamanho de la Columna Matriz: "; cin >> col;
	iniciar(matA);
	getramdon(matA);
	iniciar(matB);
	getramdon(matB);
	iniciar(matC);
	cout << "\n Imprimiendo la MatrizA antes de sumar";
	imprimir(matA);
	cout << "\n Imprimiendo la MatrizB antes de sumar";
	imprimir(matB);
	cout << "\n Sumando Matrices con Hilos";
	delimitador dthread1;
  	delimitador dthread2;

	  dthread1.inicio = 0;
	  dthread1.fin = fil/2;

	  dthread2.inicio = dthread1.fin;
	  dthread2.fin = fil;
   

  pthread_create (&thr1, NULL, suma, &dthread1);
  pthread_create (&thr2, NULL, suma, &dthread2);

  pthread_join(thr1, NULL);
  pthread_join(thr2, NULL);

	cout << "\n Imprimiendo Matriz resultante\n";
	imprimir(matC);
	return 0;
}

void *suma(void *param) {
  int fila_ini, fila_fin;
  delimitador *datos;

  datos = (delimitador *) param;

  fila_ini = datos -> inicio;
  fila_fin = datos -> fin;

	for (int i = fila_ini; i < fila_fin; i++)
	    for (int j = 0; j < col; ++j) {
			matC[i][j] = matA[i][j] + matB[i][j];
		}

	pthread_exit(NULL);
}

void imprimir(int **matriz) {
	for(int i=0;i<fil;++i) {
		for(int j=0;j<col;++j)
			cout << matriz[i][j] << " ";
		cout << endl;
	}
	printf("\n");
}