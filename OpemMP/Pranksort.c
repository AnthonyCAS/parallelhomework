#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include <omp.h>
#define N 20
#define NUM_THREADS 1
 
int main(int argc, char *argv[])  {
   int x[N], y[N];
   int i, j, my_num, my_place,size,rank,pieces,tid,n_threads;
 
   for (i=0; i<N; i++)
        x[i] = N - i;
   MPI_Status status;
   MPI_Init(&argc,&argv);
   MPI_Comm_size(MPI_COMM_WORLD,&size);
   MPI_Comm_rank(MPI_COMM_WORLD,&rank);

   MPI_Bcast(&x, N, MPI_INT, 0, MPI_COMM_WORLD);
   pieces=N/size;
   MPI_Bcast(&pieces, 1, MPI_INT, 0, MPI_COMM_WORLD);
   omp_set_num_threads(NUM_THREADS);
   #pragma omp parallel private(j,i,tid,my_num,my_place)
   {
     n_threads=omp_get_num_procs();
     tid=omp_get_thread_num();
     for (j=rank*pieces+tid*pieces/n_threads; j<rank*pieces+pieces/n_threads+tid*pieces/n_threads; j++) {
        my_num = x[j];
        my_place = 0;
        for (i=0; i<N; i++)
            if ( my_num > x[i] )
              my_place++;

          if(rank!=0){
             MPI_Send(&my_place, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
             MPI_Send(&my_num, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
          }
          else {
             //y[my_place]=my_num;
             for (i=1;i<size;i++){
               MPI_Recv(&my_place, 1, MPI_INT, i, 0, MPI_COMM_WORLD, &status);
               MPI_Recv(&my_num, 1, MPI_INT, i, 0, MPI_COMM_WORLD, &status);
               y[my_place]=my_num;
             }
          }
     }
   }
   if(rank==0){
      for (i=0; i<N; i++)
        printf("%d\n", y[i]);
   }
   MPI_Finalize();
   return 0;
}