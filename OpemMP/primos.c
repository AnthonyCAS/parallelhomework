#include <omp.h>
#include <stdio.h>
#include <stdlib.h> 
int sum = 0;
#define val 1000000
int is_primo(int n){
   if (n < 2)
      return 0;
   int i;
   for (i = 2; i < n; ++i){
      if ((n % i) == 0)
         return 0;
   }
   return 1;
}
void contar(){

   int count = 0,i;
//   #pragma omp parallel num_threads(8) {
      #pragma omp parallel for schedule(dynamic)
      for (i = 0; i < val; ++i){
         if (is_primo(i)) {
            #pragma omp atomic
               ++count;
         }
      }

   printf("\n Numeros primos: %d\n",count);
}
int main(int argc, int**argv) {
	int p=1;   
	printf("\nNumero N: %d\n",val);
    #pragma omp parallel for private(p) reduction(+:sum)
	for (p = 1; p <= val; ++p){
   		if(is_primo(p)==1){
			printf("\n%d\n",p);
			sum+=p;
		}
   		  
	}
	printf("\nLa suma de los N primeros numeros es: %d\n",sum);
        contar();
	return 0;
}
