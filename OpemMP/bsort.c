/*Bucket sort using openmp and mpi, here i am using two algorithm to sort each bucket: seelction sort 
and rank sort,
the input's number can be getting randomly (0-2^n)or by a file. By Anthony Ccapira*/
 
#include <stdio.h>
#include <stdlib.h>    /* for random function */
#include "mpi.h"
#define NUM_THREADS 2
void Generate_num_randomly(long int [], int, int, int);
void Selection_sort(long int [], int);
int  Get_minpos(long int [], int);
void Rank_sort(long int[],int);
int check_number(long int , long int [],int  );

int main(int argc, char* argv[]) {
    long int  * global_array; //array to sort
    long int  * local_array;  //each process have its own local_array
    int       tam = 100;      // default is 100 elements to sort 
    int       proportion;     // variable to set the proportion, tam/procesos 
    int       procesos;       // number of processes created
    int       id_proceso;     // id for each mpi processes
    int       i;              // for for loops, in c it is needed
    double    start, stop;    // for timing 
    //starting  mpi
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &procesos);
    MPI_Comm_rank(MPI_COMM_WORLD, &id_proceso);
    tam = 100;
    if(argc > 1)
      tam = atoi(argv[1]);  // here i am getting the array dimension
    proportion = tam/procesos;
    if (id_proceso == 0) {

        start = MPI_Wtime();  /* Timing */
        /* check if parameters are valid */
        if (tam % procesos != 0) {
          	fprintf(stderr,"El tamanho del array tiene que ser divisible por el numero de procesos.\n");
          	MPI_Abort( MPI_COMM_WORLD, 2 );
          	exit(1);
        }
  	
        /* make big array */
        global_array = malloc(tam*sizeof(long int));
        if (global_array==NULL) {
          	fprintf(stderr, "Fallo en la asignacion de memoria para el array global!!\n");
          	MPI_Abort( MPI_COMM_WORLD, 3 );
          	exit(0);
        }
        printf("\nElementos en el array: %d; cada proceso ordena: %d elementos.\n", tam, proportion);
        Generate_num_randomly(global_array, tam, proportion, procesos);
    }

    
      local_array = malloc(proportion*sizeof(long int));
      if (local_array==NULL) {
          fprintf(stderr, "Fallo en la asignacion de memoria para el array local!!\n");
          MPI_Abort( MPI_COMM_WORLD, 4 );
          exit(0);
      }

      /*  here i am sending a global array proportion to other slave processes */
      MPI_Scatter(global_array,   proportion, MPI_LONG, local_array, proportion, MPI_LONG, 0, MPI_COMM_WORLD);  

      //testing
      /*printf("\n proceso [%d] , array local: ",id_proceso);
      for (i = 0; i < proportion; ++i) {
          printf(" %7ld", local_array[i]);
      }*/
      //Selection_sort(local_array, proportion);
      Rank_sort(local_array,proportion);
      //testing
      printf("\n proceso [%d], array local: ",id_proceso);
      for (i = 0; i < proportion; ++i) {
          printf(" %7ld", local_array[i]);
      }
      MPI_Gather(local_array, proportion, MPI_LONG, global_array,   proportion, MPI_LONG, 0, MPI_COMM_WORLD);

    
    stop = MPI_Wtime();
    if (id_proceso == 0) {
 
        printf("\n Despues de ordenar:\n");
        for(i = 0; i < tam ; ++i) 
          printf("%7ld %s", global_array[i], i%8==7 ? "\n "  : " "); 
        printf("\n\nTiempo para ordenar con %d procesos: %lf msecs\n", procesos, (stop - start)/0.001);
    }

    free(local_array);
    if (id_proceso==0) 
      free(global_array);
    MPI_Finalize();

}
   

/*****************************************************************/
void Generate_num_randomly(long int  global_array[], int tam, int proportion, int processes) {

    int i, j;
    long int val;
    srand(time(NULL));
    printf("\n Creando los numeros antes de ordenar, para cada bucket:\n");
    for (i = 0; i < processes; ++i) {
      printf("\n P[%d]: ", i); 
      for (j = 0; j < proportion; ++j) {
            do{
              val = rand () % ((i*proportion+proportion-1)-(i*proportion)+1) + (i*proportion);
            }while(check_number(val, global_array,i*proportion+j) == 1);
            //global_array[i*proportion+j] = random() % (2*tam/processes) + (i*2*tam/processes);
          	global_array[i*proportion+j] = val;
          	printf("%7ld %s", global_array[i*proportion+j], j%8==7 ? "\n " :" "); 
      }
      printf("\n"); 
    }
}

/*****************************************************************/
void Selection_sort(long int array[], int  size) {
    int  eff_size, minpos;
    long int temp;

    for(eff_size = size; eff_size > 1; eff_size--) {
        minpos = Get_minpos(array, eff_size);
        temp = array[minpos];
        array[minpos] = array[eff_size-1];
        array[eff_size-1] = temp;
    }
}
/**************search********************************************/
int check_number(long int num, long int array[],int tam){
  int u;
  for(u=0;u<tam;++u)
    if(array[u] == num)
      return 1;
  return 0;
}
/********************************************************************/
//rank sort
void Rank_sort(long int array[],int N) {

   long int y[N];
   int i, j, my_num, my_place,tid;
   for(i=0;i<N;++i)
      y[i]=array[i];
    omp_set_num_threads(NUM_THREADS);
    #pragma omp parallel private(j,i,tid,my_num,my_place)
    {

       tid=omp_get_thread_num();
       //printf("\n hola : %d\n",tid);
       for (j=tid*N/NUM_THREADS; j<tid*N/NUM_THREADS+N/NUM_THREADS; ++j) {
         my_num = y[j];
         my_place = 0;
         for (i=0; i<N; i++){
            if ( my_num > y[i] )
                my_place++;
         }
         array[my_place] = my_num;
       }
    }
  /*printf("\n ordened array local: ");
      for (i = 0; i < N; ++i) {
          printf(" %7ld", array[i]);
      }*/
}
/* This function return the index of the smallest element left */
int Get_minpos(long int array[], int size) {
  int i, minpos = 0;
  for (i = 0; i < size; ++i)
      minpos = array[i] > array[minpos] ? i: minpos; // arithmetic if
  return minpos;
}
