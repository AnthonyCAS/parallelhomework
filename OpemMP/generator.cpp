#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <fstream>
#include <iostream>
using namespace std;
    
int main(int argc, char* argv[]) {
      int size = 0;
      if(argc>0)
      	size = strtol(argv[1], NULL, 10);;
      cout << "Tamanho: "<< size << endl;
      unsigned int *array= new unsigned int [size];
      unsigned int c;
      srand(time(NULL));   // preparing random function
      //srand(1); // for the same results
   
      for(c = 0 ; c < size ; c++)  //
          *(array+c) = rand()%size+1;
      ofstream fd;
      fd.open("lista.txt");
      fd << size << endl;
      for(c = 0 ; c < size ; c++)  {
          fd << *(array+c) << endl;
      }
      fd.close();
      return 0;
}
