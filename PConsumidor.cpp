#include <pthread.h>
#include <stdio.h>
#include <iostream>
using namespace std;

#define TAM           10        /* tamaño del recursos */
#define META     30      /* datos a producir */

//declaro mis funciones
void * Productor(void*);
void * Consumidor(void*);

pthread_mutex_t mutex;  /* mutex para controlar el acceso */
pthread_cond_t no_lleno;    /* esperar si no está lleno */
pthread_cond_t no_vacio;    /* esperar si no está vacío */
int contador=0;  /* número de elementos en los recursos */

int recursos[TAM]; // recursos 



/* código del productor */
void* Productor(void*) {
    int dato,pos = 0;

    for(int i=0; i<META; i++ ) {
        dato = i;                        
        pthread_mutex_lock(&mutex);    
        while (contador == TAM){  
        	cout << "\n wait productor";      
            pthread_cond_wait(&no_lleno, &mutex); 
        }
        recursos[pos] = dato;
        pos = (pos + 1) % TAM;
        contador = contador + 1;
        if (contador > 1)
            pthread_cond_signal(&no_vacio);
        pthread_mutex_unlock(&mutex);
        printf("\n Produce %d \n", dato);      
    }
    pthread_exit(NULL);
}
/* código del consumidor */
void* Consumidor(void* )  {
    int dato,pos = 0;
    for(int i=0; i<META; ++i) {
        pthread_mutex_lock(&mutex);        
        while (contador == 0){    
                /* si recursos vacío */
            pthread_cond_wait(&no_vacio, &mutex); 
            cout << "\n wait comsumidor";
    	}
        dato = recursos[pos];
        pos = (pos + 1) % TAM;
        contador = contador - 1 ;
        if (contador < TAM );
            pthread_cond_signal(&no_lleno); 
        pthread_mutex_unlock(&mutex);
        printf("\n Consume %d \n", dato);   
    }
    pthread_exit(NULL);
}

int main(){

    pthread_t th1, th2;

    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&no_lleno, NULL);
    pthread_cond_init(&no_vacio, NULL);

    pthread_create(&th1, NULL, Productor, NULL);
    pthread_create(&th2, NULL, Consumidor, NULL);

    pthread_join(th1, NULL);
    pthread_join(th2, NULL);

    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&no_lleno);
    pthread_cond_destroy(&no_vacio);

    return 0;
}

